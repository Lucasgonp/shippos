//
//  AlertasCadastrarCartaoswift.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 28/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit

extension CadastrarCartaoViewController {
    func alertaDadosIncompletos() {
        let alertasClass = AlertaClass()
        let alertaController = alertasClass.alertaInformacoesIncompletas()
        self.present( alertaController , animated: true, completion: nil )
    }
}

