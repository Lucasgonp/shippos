//
//  CheckoutViewController.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 28/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

class CheckoutViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var produtoLabel: UILabel!
    @IBOutlet weak var valorEstimadoLabel: UILabel!
    @IBOutlet weak var taxaEntregaLabel: UILabel!
    @IBOutlet weak var distanciaLabel: UILabel!
    @IBOutlet weak var valorTotalLabel: UILabel!
    @IBOutlet weak var endUsuarioLabel: UILabel!
    @IBOutlet weak var numeroDoCartaoLabel: UILabel!
    @IBOutlet weak var bandeirinhaCartao: UIImageView!
    @IBOutlet weak var botaoFinalizarPedido: UIButton!
    
    var receberNomeEstabelecimento: String?
    var receberEndEstabelecimento: String?
    var receberCoordinateEstabelecimento: CLLocationCoordinate2D?
    var estabelecimento: GMSPlace?
    var receberProdutoDigitado: String?
    var receberValorEstimado: Double?
    var receberTaxa: Double?
    var receberDistancia: Double?
    var receberEndUsuario: String?
    var receberValorTotal: Double?
    
    var distanciaFormatada: String?
    
    var cvvDoCartao: String?
    var validadeDoCartao: String?
    var numeroDoCartao: String?
    
    var valorTotalFeedbackAPI: Double?
    var mensagemFeedbackAPI: String?
    
    let gerenciadorLocalizacao = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if receberNomeEstabelecimento != nil {
            self.mandarDadosParaLabels()
            consultarEndereco()
            recuperarCartao()}
        
        gerenciadorLocalizacao.delegate = self
        gerenciadorLocalizacao.requestWhenInUseAuthorization()}
    
    @IBAction func botaoPagamentoPressionado(_ sender: Any) {
        
        if numeroDoCartaoLabel.text == "" {
            cadastrarCartaoVC()
        }else{
            let alerta = alertaEditarCartao()
            present(alerta, animated: true, completion: nil)}}
    
    // Finalizar Pedido
    @IBAction func finalizarPedidoPressionar(_ sender: Any) {
        if numeroDoCartao != nil {
        validarCompraAPI()
        finalizarCompraVC()
        }else{
            let alertaClass = AlertaClass()
            let alertaController = alertaClass.alertaInformacoesIncompletas()
            self.present( alertaController , animated: true, completion: nil )
        }}}
