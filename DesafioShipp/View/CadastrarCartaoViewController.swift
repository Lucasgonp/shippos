//
//  CadastrarCartaoViewController.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 28/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit
import GooglePlaces

class CadastrarCartaoViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var numeroCartaoTextField: UITextField!
    @IBOutlet weak var nomeCartaoTextField: UITextField!
    @IBOutlet weak var validadeCartaoTextField: UITextField!
    @IBOutlet weak var cVVCartaoTextField: UITextField!
    @IBOutlet weak var botaoConfirmar: UIButton!
    @IBOutlet weak var bandeiraCadastrarCartao: UIImageView!
    
    var nomeEstabFinal: String?
    var enderecoEstabFinal: String?
    var produtoFinal: String?
    var distanciaFinal: Double?
    var valorEntregaFinal: Double?
    var valorEstimadoFinal: Double?
    var valorTotalFinal: Double?
    var coordEstabFinal: CLLocationCoordinate2D?
    
    var previousTextFieldContent: String?
    var previousSelection: UITextRange?
    
    var estabelecimento: GMSPlace?
    
    override func viewDidLoad() {
        super.viewDidLoad()
            if numeroCartaoTextField != nil {
        recuperarDadosCartao()}
        passarDadosDoCartao()
        if numeroCartaoTextField.text != "" {
        bandeiraCadastrarCartao.image = #imageLiteral(resourceName: "mastercard")
        }else{ bandeiraCadastrarCartao.image = .none }}
    
    @IBAction func apertarConfirmar(_ sender: Any) {
        if numeroCartaoTextField.text!.count < 16 {
            alertaDadosIncompletos()
        } else if nomeCartaoTextField.text!.count < 3 {
            alertaDadosIncompletos()
        } else if validadeCartaoTextField.text!.count <  4 {
            alertaDadosIncompletos()
        } else if cVVCartaoTextField.text!.count <  3 {
            alertaDadosIncompletos()
        } else {
                        salvarDadosCartao()
                         checkoutVC()}}}
