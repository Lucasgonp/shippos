//
//  PrecoViewController.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 26/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

class PrecoViewController: ProdutoViewController {
    
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var produtoLabel: UILabel!
    @IBOutlet weak var precoDigitado: UITextField!
    @IBOutlet weak var botaoFPPreco: UIButton!
    
    var receberNomeEstabelecimento: String?
    var receberEndEstabelecimento: String?
    var rebeberCoordinateEstabelecimento: CLLocationCoordinate2D?
    var receberProdutoDigitado: String?
    var receberCoordinateUsuario: CLLocationCoordinate2D?
    var valorTaxa: Double?
    var valorProduto: Double?
    var valorTotal: Double?
    var distancia: Double?
    
    let toolbarPreco = UIToolbar()
    
    var placeAPI: GMSPlace?
    
    var latUS: CLLocationDegrees?
    var longUS: CLLocationDegrees?
    
    lazy var currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "pt_BR")
        //formatter.currencySymbol = "R$"
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if receberNomeEstabelecimento != nil {
            
        // Preco maximo Textfield
        precoDigitado.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        precoDigitado.delegate = self
            
        // Passar os dados para as labels
        recuperarNomeEstabelecimentoPrecoVC()
        recuperarEndEstabelecimentoPrecoVC()
        produtoLabel.text = receberProdutoDigitado
        
        // passar coord
        rebeberCoordinateEstabelecimento = coordinateEstabelecimento
        receberCoordinateUsuario = gerenciadorLocalizacao.location?.coordinate
            
        // Setup Teclado (botão confirmar)
        let toolBarItem = setupKeyboardPreco()
        precoDigitado.inputAccessoryView = toolBarItem }}
    
    // Formatar preco BR currency
    @IBAction func digitandoPreco(_ sender: UITextField) {
    guard let text = sender.text else { return }
        let array = text.compactMap({ Int(String($0)) })
        let num = Double(array.reduce(0, {($0 * 10) + $1})) / 100
        precoDigitado.text = currencyFormatter.string(from: NSNumber(value: num))
        validarDigitosPreco()}
    
    // Recuperar nome Estabelecimento
    func recuperarNomeEstabelecimentoPrecoVC() {
      if receberNomeEstabelecimento != nil {
        nomeLabel.text = receberNomeEstabelecimento
        }else{ nomeLabel.text = "Erro" }}
    
    // Recuperar endereco Estabelecimento 
    func recuperarEndEstabelecimentoPrecoVC() {
      if receberEndEstabelecimento != nil {
        endLabel.text = receberEndEstabelecimento
        }else{ endLabel.text = "Erro" }}
    
    // Tratar limite de preco digitado
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else { return false }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 12 }
    
    // Acao botao confirmar
    @IBAction func confirmarPreco() {
        validateNextViewController()}
    
    // Acao botao cancelar
    @objc func cancelarPreco() {
        view.endEditing(true)}
    
    @IBAction func FinalizarPedidoPressionado(_ sender: Any) {
        validateNextViewController()}}

   // Formatar preco String para Double para request POST
extension String {
    func removeFormatAmount() -> Double {
        let formatter = NumberFormatter()

        formatter.locale = Locale(identifier: "pt_BR")
        formatter.numberStyle = .currency
        formatter.currencySymbol = "R$"
        formatter.decimalSeparator = "."
        return formatter.number(from: self) as! Double? ?? 0}}

