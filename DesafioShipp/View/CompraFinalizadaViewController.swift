//
//  CompraFinalizadaViewController.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 29/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit

class CompraFinalizadaViewController: UIViewController {

    var nomeProduto: String?
    var distancia: Double?
    var distanciaFormatada: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if distancia != nil {
            formatarDistancia()}}
    
    @IBAction func confirmarPressionado(_ sender: Any) {
    alertaInformacoesFinais()}
    
    func formatarDistancia() {
        let formatterDistancia = NumberFormatter()
        formatterDistancia.locale = Locale(identifier: "pt_BR")
        formatterDistancia.numberStyle = .decimal
            if let distanciaFormat = formatterDistancia.string(from: distancia! as NSNumber) {
                distanciaFormatada = "\(String(describing: distanciaFormat)) Km"
                }else{ distanciaFormatada = "Erro ao formatar distancia" }}}

