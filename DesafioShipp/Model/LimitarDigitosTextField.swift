//
//  LimitarDigitosTextField.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 28/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit

extension PrecoViewController {
    
    // Limitar numero de digitos preço
    func limitarDigitos(textField: UITextField!, maxLength: Int) {
        if textField.text!.count > maxLength {
            textField.deleteBackward()
        }}}

extension CadastrarCartaoViewController {
    
    // Limitar numero de digitos
    func limitarDigitos(textField: UITextField!, maxLength: Int) {
        if textField.text!.count > maxLength {
            textField.deleteBackward()
        }}}
