//
//  FormatarCartaoDeCredito.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 28/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit

extension CadastrarCartaoViewController {
    
    func passarDadosDoCartao() {
        numeroCartaoTextField.addTarget(self, action: #selector(reformatAsCardNumber), for: .editingChanged)
        validadeCartaoTextField.addTarget(self, action: #selector(reformatAsDate), for: .editingChanged)
        cVVCartaoTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        cVVCartaoTextField.delegate = self
    }
    
    // Formatar Numero Cartao
    func textFieldNumeroCartao(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        previousTextFieldContent = textField.text;
        previousSelection = textField.selectedTextRange;
        return true
    }
    
    @objc func reformatAsCardNumber(textField: UITextField) {
        var targetCursorPosition = 0
        if let startPosition = textField.selectedTextRange?.start {
            targetCursorPosition = textField.offset(from: textField.beginningOfDocument, to: startPosition)
        }
        var cardNumberWithoutSpaces = ""
        if let text = textField.text {
            cardNumberWithoutSpaces = self.removeNonDigits(string: text, andPreserveCursorPosition: &targetCursorPosition)
        }
        
        // Limitar numero de digitos numero do cartao
            if cardNumberWithoutSpaces.count > 16 {
        limitarDigitos(textField: numeroCartaoTextField, maxLength: 16)
        return
        }
        
        let cardNumberWithSpaces = self.insertCreditCardSpaces(cardNumberWithoutSpaces, preserveCursorPosition: &targetCursorPosition)
        textField.text = cardNumberWithSpaces

        if let targetPosition = textField.position(from: textField.beginningOfDocument, offset: targetCursorPosition) {
            textField.selectedTextRange = textField.textRange(from: targetPosition, to: targetPosition)
        }}

    func removeNonDigits(string: String, andPreserveCursorPosition cursorPosition: inout Int) -> String {
        var digitsOnlyString = ""
        let originalCursorPosition = cursorPosition

        for i in Swift.stride(from: 0, to: string.count, by: 1) {
            let characterToAdd = string[string.index(string.startIndex, offsetBy: i)]
            if characterToAdd >= "0" && characterToAdd <= "9" {
                digitsOnlyString.append(characterToAdd)
            }
            else if i < originalCursorPosition {
                cursorPosition -= 1
            }}
        return digitsOnlyString
    }
    
    func insertCreditCardSpaces(_ string: String, preserveCursorPosition cursorPosition: inout Int) -> String {
        let is4444 = true
        var stringWithAddedSpaces = ""
        let cursorPositionInSpacelessString = cursorPosition
        for i in 0..<string.count {
            let needs4444Spacing = (is4444 && i > 0 && (i % 4) == 0)
            if needs4444Spacing {
                stringWithAddedSpaces.append(" ")
                if i < cursorPositionInSpacelessString {
                    cursorPosition += 1
                }}
            let characterToAdd = string[string.index(string.startIndex, offsetBy:i)]
            stringWithAddedSpaces.append(characterToAdd)
        }
        return stringWithAddedSpaces
    }
    
    // Formatar Validade Cartao
    func textFieldValidade(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        previousTextFieldContent = textField.text;
        previousSelection = textField.selectedTextRange;
        return true
    }

    @objc func reformatAsDate(textField: UITextField) {
        var targetCursorPosition = 0
        if let startPosition = textField.selectedTextRange?.start {
            targetCursorPosition = textField.offset(from: textField.beginningOfDocument, to: startPosition)
        }

        var cardNumberWithoutSpaces = ""
        if let text = textField.text {
            cardNumberWithoutSpaces = self.removeNonDigitsDate(string: text, andPreserveCursorPosition: &targetCursorPosition)
        }

        // Limitar numero de digitos numero do cartao
            if cardNumberWithoutSpaces.count > 4 {
        limitarDigitos(textField: validadeCartaoTextField, maxLength: 4)
        return }
        

        let cardNumberWithSpaces = self.insertDateBar(cardNumberWithoutSpaces, preserveCursorPosition: &targetCursorPosition)
        textField.text = cardNumberWithSpaces

        if let targetPosition = textField.position(from: textField.beginningOfDocument, offset: targetCursorPosition) {
            textField.selectedTextRange = textField.textRange(from: targetPosition, to: targetPosition)
        }}

    func removeNonDigitsDate(string: String, andPreserveCursorPosition cursorPosition: inout Int) -> String {
        var digitsOnlyString = ""
        let originalCursorPosition = cursorPosition

        for i in Swift.stride(from: 0, to: string.count, by: 1) {
            let characterToAdd = string[string.index(string.startIndex, offsetBy: i)]
            if characterToAdd >= "0" && characterToAdd <= "9" {
                digitsOnlyString.append(characterToAdd)
            }
            else if i < originalCursorPosition {
                cursorPosition -= 1
            }}
        return digitsOnlyString
    }

    func insertDateBar(_ string: String, preserveCursorPosition cursorPosition: inout Int) -> String {
        
        let format4444 = true

        var stringWithAddedSpaces = ""
        let cursorPositionInSpacelessString = cursorPosition

        for i in 0..<string.count {
            
            let needs4444Spacing = (format4444 && i > 0 && (i % 2) == 0)

            if needs4444Spacing {
                stringWithAddedSpaces.append("/")

                if i < cursorPositionInSpacelessString {
                    cursorPosition += 1
                }}
            let characterToAdd = string[string.index(string.startIndex, offsetBy:i)]
            stringWithAddedSpaces.append(characterToAdd)
        }
        return stringWithAddedSpaces
    }
    
    // Fechar keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }}
