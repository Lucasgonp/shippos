//
//  SetButtonsColors.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 29/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit

extension ProdutoViewController {
    
    // Digitando Produto
    @IBAction func digitandoProduto(_ sender: Any) {
        validarDigitos()}
    
    func validarDigitos() {
        if produtoDigitado.text!.count >= 3 {
        let corRoxo = UIColor(rgb: 0x7967FF)
            botaoFPProduto.backgroundColor = corRoxo
            botaoFPProduto.setTitle("Continuar", for: .normal)
        }else{
        let colorCinza = UIColor(rgb: 0xDCDCDC)
            botaoFPProduto.backgroundColor = colorCinza
            botaoFPProduto.setTitle("Finalizar Pedido", for: .normal)}}}

extension PrecoViewController {
    
    func validarDigitosPreco() {
        if precoDigitado.text! != "" && precoDigitado.text! != "R$ 0,00" {
        let corRoxo = UIColor(rgb: 0x7967FF)
            botaoFPPreco.backgroundColor = corRoxo
            botaoFPPreco.setTitle("Continuar", for: .normal)
        }else{
        let colorCinza = UIColor(rgb: 0xDCDCDC)
            botaoFPPreco.backgroundColor = colorCinza
            botaoFPPreco.setTitle("Finalizar Pedido", for: .normal)}}}

extension CadastrarCartaoViewController {
    
    // Digitando CVV
    @IBAction func digitandoCVV(_ sender: Any) {
        limitarDigitos(textField: cVVCartaoTextField, maxLength: 3)
        validarDigitos()}
    
    // Digitando Numero
    @IBAction func digitandoNumeroCartao(_ sender: Any) {
        if numeroCartaoTextField.text!.count >= 4 {
            bandeiraCadastrarCartao.image = #imageLiteral(resourceName: "mastercard")
        }else{
            bandeiraCadastrarCartao.image = .none
        }
        validarDigitos()}
    
    // Digitando Nome
    @IBAction func digitandoNome(_ sender: Any) {
        validarDigitos()}
    
    // Digitando Validade
    @IBAction func digitandoValidade(_ sender: Any) {
        validarDigitos()}
    
    func validarDigitos() {
        if numeroCartaoTextField.text!.count == 19 && nomeCartaoTextField.text!.count >= 3 && validadeCartaoTextField.text!.count == 5 && cVVCartaoTextField.text!.count == 3 {
        let colorAzul = UIColor(rgb: 0x7967FF)
            botaoConfirmar.backgroundColor = colorAzul
        }else{
        let colorCinza = UIColor(rgb: 0xDCDCDC)
            botaoConfirmar.backgroundColor = colorCinza
        }
    }}

extension UIColor {
convenience init(red: Int, green: Int, blue: Int) {
    assert(red >= 0 && red <= 255, "Componente vermelho inválido")
    assert(green >= 0 && green <= 255, "Componente verde inválido")
    assert(blue >= 0 && blue <= 255, "Componente azul inválido")

    self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)}

convenience init(rgb: Int) {
    self.init(
        red: (rgb >> 16) & 0xFF,
        green: (rgb >> 8) & 0xFF,
        blue: rgb & 0xFF )}}
