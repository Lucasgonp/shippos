//
//  DadosEstabelecimento.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 26/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit
import CoreLocation

class DadosEstabelecimentos {
    var nomeEstabelecimento: String?
    var endEstabelecimento: String?
    var coordinateEStabelecimanto: CLLocationCoordinate2D?
    
    init(nomeEstab: String, endEstab: String, coordEstab: CLLocationCoordinate2D) {
        self.nomeEstabelecimento = nomeEstab
        self.endEstabelecimento = endEstab
        self.coordinateEStabelecimanto = coordEstab
    }}

extension DadosEstabelecimentos {
    func recuperarNomeEstab() -> String {
        if nomeEstabelecimento != nil {
            return nomeEstabelecimento!
        }else{
            print("Erro ao recuperar nome Estab")
            return "Erro ao recuperar nome Estab"
        }}
    
    func recuperarEndEstab() -> String {
        if endEstabelecimento != nil {
            return endEstabelecimento!
        }else{
            print("Erro ao recuperar end Estab")
            return "Erro ao recuperar end Estab"
        }}
    
    func recuperarCoordinateEstab() -> CLLocationCoordinate2D {
        if coordinateEStabelecimanto != nil {
            return coordinateEStabelecimanto!
        }else{
            print("Erro ao recuperar coord Estab")
            return coordinateEStabelecimanto! }}}

