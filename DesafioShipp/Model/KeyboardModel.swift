//
//  keyboardModel.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 26/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit

extension ProdutoViewController {
    
    func setupKeyboardProduto() -> UIToolbar {
    toolbarProduto.barStyle = UIBarStyle.default
    toolbarProduto.items = [
        UIBarButtonItem(title: "Cancelar", style: UIBarButtonItem.Style.done, target: self, action: #selector(cancelarProduto)),
        UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
        UIBarButtonItem(title: "Confirmar", style: UIBarButtonItem.Style.done, target: self, action: #selector(confirmarProduto))]
    toolbarProduto.sizeToFit()
    return toolbarProduto }
    
    // Fechar keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true) }}

extension PrecoViewController {
    func setupKeyboardPreco() -> UIToolbar {
    toolbarPreco.barStyle = UIBarStyle.default
    toolbarPreco.items = [
        UIBarButtonItem(title: "Cancelar", style: UIBarButtonItem.Style.done, target: self, action: #selector(cancelarPreco)),
        UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
        UIBarButtonItem(title: "Confirmar", style: UIBarButtonItem.Style.done, target: self, action: #selector(confirmarPreco))]
    toolbarPreco.sizeToFit()
    return toolbarPreco }
    
    // Fechar keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)}}

