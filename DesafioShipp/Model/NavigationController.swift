//
//  NavigationController.swift
//  DesafioShipp
//
//  Created by Lucas Pereira on 27/10/19.
//  Copyright © 2019 Lucas Pereira. All rights reserved.
//

import UIKit

extension CheckoutViewController {
    
    func cadastrarCartaoVC() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let cadastrarCartaoVC = storyBoard.instantiateViewController(identifier: "CadastrarCartaoViewController") as! CadastrarCartaoViewController
        cadastrarCartaoVC.nomeEstabFinal = receberNomeEstabelecimento
        cadastrarCartaoVC.enderecoEstabFinal = receberEndEstabelecimento
        cadastrarCartaoVC.produtoFinal = receberProdutoDigitado
        cadastrarCartaoVC.valorEstimadoFinal = receberValorEstimado
        cadastrarCartaoVC.coordEstabFinal = receberCoordinateEstabelecimento
        cadastrarCartaoVC.estabelecimento = estabelecimento
        cadastrarCartaoVC.distanciaFinal = receberDistancia
        cadastrarCartaoVC.valorTotalFinal = receberValorTotal
        cadastrarCartaoVC.valorEntregaFinal = receberTaxa
        self.navigationController?.pushViewController(cadastrarCartaoVC, animated: true)}
    
    func finalizarCompraVC() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let compraFinalizadaVC = storyBoard.instantiateViewController(identifier: "CompraFinalizadaViewController") as! CompraFinalizadaViewController
        compraFinalizadaVC.nomeProduto = receberProdutoDigitado
        compraFinalizadaVC.distancia = receberDistancia
        self.navigationController?.pushViewController(compraFinalizadaVC, animated: true) }}

extension ProdutoViewController {
    func precoVC() {
        let storyBoard2: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let precoVC = storyBoard2.instantiateViewController(withIdentifier: "PrecoViewController") as! PrecoViewController
        precoVC.receberProdutoDigitado = produtoDigitado.text
        precoVC.receberNomeEstabelecimento = nomeEstabelecimento
        precoVC.receberEndEstabelecimento = endEstabelecimento
        precoVC.rebeberCoordinateEstabelecimento = coordinateEstabelecimento
        precoVC.placeAPI = placesAPI
        self.navigationController?.pushViewController(precoVC, animated: true) }}

extension PrecoViewController {
    func checkoutVC() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let checkoutVC = storyBoard.instantiateViewController(identifier: "CheckoutViewController") as! CheckoutViewController
        checkoutVC.receberNomeEstabelecimento = receberNomeEstabelecimento
        checkoutVC.receberEndEstabelecimento = receberEndEstabelecimento
        checkoutVC.receberProdutoDigitado = receberProdutoDigitado
        checkoutVC.receberValorEstimado = valorProduto
        checkoutVC.receberCoordinateEstabelecimento = rebeberCoordinateEstabelecimento
        checkoutVC.receberTaxa = valorTaxa
        checkoutVC.receberDistancia = distancia
        checkoutVC.receberValorTotal = valorTotal
        checkoutVC.estabelecimento = placeAPI
        self.navigationController?.pushViewController(checkoutVC, animated: true)}
    
    func validateNextViewController() {
        print(precoDigitado.text!)
        let corRoxo = UIColor(rgb: 0x7967FF)
        if botaoFPPreco.backgroundColor == corRoxo {
            consultarPrecosAPI()
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (timer) in
                self.checkoutVC()})
        }else{ alertaDadosIncompletos() }}}
    
extension CadastrarCartaoViewController {
    func checkoutVC() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let checkoutVC = storyBoard.instantiateViewController(identifier: "CheckoutViewController") as! CheckoutViewController
        checkoutVC.receberNomeEstabelecimento = nomeEstabFinal
        checkoutVC.receberEndEstabelecimento = enderecoEstabFinal
        checkoutVC.receberProdutoDigitado = produtoFinal
        checkoutVC.receberValorEstimado = valorEstimadoFinal
        checkoutVC.receberCoordinateEstabelecimento = coordEstabFinal
        checkoutVC.estabelecimento = estabelecimento
        checkoutVC.receberTaxa = valorEntregaFinal
        checkoutVC.receberDistancia = distanciaFinal
        checkoutVC.receberValorTotal = valorTotalFinal
        self.navigationController?.pushViewController(checkoutVC, animated: true)}}


